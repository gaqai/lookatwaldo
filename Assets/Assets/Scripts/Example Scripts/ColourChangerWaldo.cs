﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ColourChangerWaldo : MonoBehaviour, IGazeableObject
{
    public string scene2;
    SpriteRenderer spriteRenderer;
    Color originalColor;

    void Awake() {
        spriteRenderer = GetComponent<SpriteRenderer>();
        originalColor = spriteRenderer.color;
    }
    public void gazeAction() {
        spriteRenderer.color = Color.green;
        StartCoroutine(WaitBefore());
    }

    public void currentlyGazing() {
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }
    IEnumerator WaitBefore()
    {
        spriteRenderer.color = Color.green;
        yield return new WaitForSeconds(2);
        spriteRenderer.color = Color.green;
        if (scene2 == "exit")
        {
            Application.Quit();
        }
        else
        {
            SceneManager.LoadScene(scene2);
        }
    }
    public void stoppedGazing() {
        spriteRenderer.color = originalColor;
    }
    
    public float getGazeTime() {
        return 0.3f;
    }
}
